import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { MaterialDesign } from '../material-design/material';
import { HomeComponent } from './home/home.component';
import { PublicComponent } from './public.component';

const routes: Routes = [
  {
    path:"",
    component:PublicComponent,
    children:[
      {
        path:"home",
        component:HomeComponent

      }
    ]
  }
]



@NgModule({
  declarations: [PublicComponent, HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialDesign
  ]
})
export class PublicModule { }
